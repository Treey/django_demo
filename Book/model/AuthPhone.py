from django.db import models

class AuthPhone(models.Model):

    """认证手机号"""
    user_id = models.CharField(max_length=64)
    user_name = models.CharField(max_length=64)
    user_phone = models.CharField(max_length=11)
    phone_pwd = models.CharField(max_length=11)
    #运营商类型:1、移动，2、联通，3、电信
    phone_type = models.IntegerField(max_length=1)
    #认证状态:1、未审核，2、审核中、3、认证通过
    auth_status = models.IntegerField(max_length=1)
    auth_desc = models.CharField(max_length=256)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    is_delete = models.IntegerField(max_length=1,default=0)
    class Meta:
        app_label='Book'