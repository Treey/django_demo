from django.db import models


# Create your models here.

class BookInfo(models.Model):
    """书籍类"""
    name = models.CharField(max_length=10)

    def __str__(self):
        """以字符输出"""
        return self.name


class PeopleInfo(models.Model):
    name = models.CharField(max_length=10)
    book = models.ForeignKey(BookInfo, on_delete=models.CASCADE)

    def __str__(self):
        return self.name



