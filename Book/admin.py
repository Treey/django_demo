from django.contrib import admin
from Book.models import BookInfo
from Book.models import PeopleInfo

# Register your models here.


class PeopleAdmin(admin.ModelAdmin):
    """人物模型管理"""
    list_display = ['id', 'name', 'book']


admin.site.register(BookInfo)
admin.site.register(PeopleInfo, PeopleAdmin)