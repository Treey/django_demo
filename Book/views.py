# -*-coding:utf-8 -*-
import logging

from django.shortcuts import render

from Book.models import BookInfo

logger = logging.getLogger("django")


# Create your views here.

def booklist(request):
    """加载数据信息"""

    book_list = BookInfo.objects.all()
    context = {
        'book_list': book_list
    }
    logger.debug("查询所有书籍")
    return render(request, 'Book/booklist.html', context)


def peoplelist(request, book_id):
    book = BookInfo.objects.get(id=book_id)
    people_list = book.peopleinfo_set.all()
    context={
        'people_list':people_list
    }
    logger.info("书籍中的人物")
    return render(request, 'Book/peoplelist.html',context)

def test(request):

    """测试请求逻辑"""

    # response = HttpResponse("first python view")
    # return response、

    # 调用模板：未接受参数
    context = {
        'test': "测试"
    }
    # 调用模板接受上下文参数
    return render(request, 'Book/test.html', context)
