from django.conf.urls import url
from Book.views import test, booklist, peoplelist

urlpatterns = [

    url(r'^test/$', test),
    url(r'^booklist/$', booklist),
    url(r'^booklist/(\d+)/$', peoplelist),
    url(r'/auth/phone/')
]